---
layout: default
title: "home"
---

# $ cat {{site.title}}

########################################

> a world, but for birb

########################################

### h.1 world

* soon™️

### h.2 characters

* [aeolus](/c/aeolus)

### h.3 projects

* this website
* [wpafw.org](https://wpafw.org) - wpafw's website
* [fox.birb.world](https://fox.birb.world) - my partner's site, written by [@fanfallafox](https://twitter.com/fanfallafox) and mentored by me

########################################